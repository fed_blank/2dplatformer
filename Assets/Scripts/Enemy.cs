﻿using UnityEngine;

public class Enemy : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private float _damagePoints = 10f;

    #endregion

    #region Virtual Methods

    public virtual void Damage()
    {
        if (Player.Instance.IsAlive)
        {
            GameSession.Instance.DecreaseHealthPoints(_damagePoints);
        }
    }

    #endregion

    #region Unity Methods

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            Damage();
        }
    }

    #endregion
}
