﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private float _moveSpeed = 3f;

    #endregion

    #region Private Fields

    Rigidbody2D _rigidbody2D;

    #endregion

    #region Unity Methods

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _rigidbody2D.velocity = Vector2.right * ((transform.localScale.x > 0) ? _moveSpeed : -_moveSpeed)
            + Vector2.up * _rigidbody2D.velocity.y;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        transform.localScale = Vector2.right * (-transform.localScale.x) + Vector2.up * transform.localScale.y;
    }

    #endregion
}
