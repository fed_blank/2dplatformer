﻿using UnityEngine;
using UnityEngine.UI;

public class GamePageUIController : PageUI
{

    #region Unity Editor

    [SerializeField] private Slider _healthSlider;

    #endregion

    #region Unty Methods

   /*private void Start()
    {
        RenderHealthSlider();
    }*/

    #endregion

    #region Public Methods

    public void RenderHealthSlider()
    {
        _healthSlider.value = GameSession.Instance.CurrentHealthPoints / GameSession.Instance.MaxHealthPoints;
    }

    #endregion

    #region Override Methods

    public override void Show()
    {
        base.Show();
        RenderHealthSlider();
    }

    #endregion

}
