﻿using UnityEngine;

public class GameSession : MonoBehaviour
{

    #region Properties

    public static GameSession Instance
    {
        get
        {
            return instance;
        }
        set
        {
            instance = value;
        }
    }

    public float MaxHealthPoints
    {
        get
        {
            return _maxHealthPoints;
        }
    }

    public float CurrentHealthPoints
    {
        get
        {
            return _healthPoints;
        }
    }

    #endregion

    #region Unity Editor

    [SerializeField] private float _maxHealthPoints = 100f;

    #endregion

    #region Private Fields

    private static GameSession instance = null;
    private UIController _uiController;
    private GamePageUIController _gamePageUIController;
    private float _coins;
    private float _healthPoints;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    private void Start()
    {
        _healthPoints = _maxHealthPoints;
        _uiController = FindObjectOfType<UIController>();
        _uiController.OpenGamePage();
        _gamePageUIController = (GamePageUIController)_uiController.GetGamePage();
    }

    #endregion

    #region Public Methods

    public void DecreaseHealthPoints(float value)
    {
        _healthPoints -= value;
        if (_healthPoints <= 0)
        {
            Player.Instance.Die();
            _healthPoints = 0;
            Invoke("LostGame", 2f);
        }
        _gamePageUIController.RenderHealthSlider();
    }

    public void EndGameSession()
    {
        Destroy(Player.Instance.gameObject);
        SceneLoadingController.LoadStartScene();
        Destroy(gameObject);
    }

    #endregion

    #region Private Invoke Method

    private void LostGame()
    {
        _gamePageUIController.Hide();
        _uiController.OpenLostGamePage();
    }

    #endregion

}
