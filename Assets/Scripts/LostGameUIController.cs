﻿using UnityEngine;
using UnityEngine.UI;

public class LostGameUIController : PageUI
{
    #region Unity Editor

    [SerializeField] private Button _goToStartMenuButton;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _goToStartMenuButton.onClick.AddListener(() =>
        {
            SceneLoadingController.LoadStartScene();
        });
    }

    #endregion
}
