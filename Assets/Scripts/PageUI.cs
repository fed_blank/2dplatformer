﻿using UnityEngine;

public class PageUI : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private GameObject _currentPage;

    #endregion

    #region Virtual Methods

    public virtual void Show()
    {
        _currentPage.SetActive(true);
    }

    public virtual void Hide()
    {
        _currentPage.SetActive(false);
    }

    #endregion
}
