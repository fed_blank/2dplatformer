﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    #region Properties

    public bool IsAlive
    {
        get
        {
            return _isAlive;
        }
        set
        {
            _isAlive = value;
        }
    }

    public static Player Instance
    {
        get
        {
            return instance;
        }
        set
        {
            instance = value;
        }
    }

    #endregion

    #region Unity Editor

    [SerializeField] private float _runSpeed = 5f;
    [SerializeField] private float _jumpSpeed = 5f;
    [SerializeField] private float _climbingSpeed = 3f;
    [SerializeField] private Vector2 _collisionWithEnemyMotionVector = Vector2.right * 5f + Vector2.up * 5f;

    #endregion

    #region Private Fields

    private static Player instance = null;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private Collider2D _collider2D;
    private bool _isAlive = true;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (Instance!=null)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
           // DontDestroyOnLoad(gameObject);
            Debug.Log("<color=green><b><i>Player first appearance</i></b></color>");
        }
    }

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _collider2D = GetComponent<Collider2D>();
    }

    private void Update()
    {
        if (IsAlive)
        {
            Run();
            Jump();
            ClimbLadder();
            FlipSpriteForHorizontalMove();
        }
    }

    #endregion

    #region Private Methods

    private void Run()
    {
        float horizontalDirection = CrossPlatformInputManager.GetAxis("Horizontal");
        _rigidbody2D.velocity = Vector2.right * (horizontalDirection * _runSpeed) + Vector2.up * _rigidbody2D.velocity.y;
        _animator.SetBool("Run", Mathf.Abs(_rigidbody2D.velocity.x) > Mathf.Epsilon);
    }

    private void ClimbLadder()
    {
        if(_collider2D.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            float verticalDirection = CrossPlatformInputManager.GetAxis("Vertical");
            _rigidbody2D.velocity = Vector2.right * _rigidbody2D.velocity.x + Vector2.up * verticalDirection * _climbingSpeed;
            _animator.SetBool("Climb", Mathf.Abs(_rigidbody2D.velocity.y) > Mathf.Epsilon);    //возможно лучше просто поставить true
        }
        else
        {
            _animator.SetBool("Climb", false); 
        }
    }

    private void Jump()
    {
        if (_collider2D.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            if (CrossPlatformInputManager.GetButtonDown("Jump"))
            {
                _rigidbody2D.velocity = Vector2.right * _rigidbody2D.velocity.x + Vector2.up * _jumpSpeed;
            }
        }
    }

    // TODO переделать эту хрень 
    private void FlipSpriteForHorizontalMove()
    {
        if (Mathf.Abs(_rigidbody2D.velocity.x) > Mathf.Epsilon)
        {
            transform.localScale = Vector2.right * Mathf.Sign(_rigidbody2D.velocity.x) + Vector2.up * transform.localScale.y;
            //Debug.Log("Sprite flipped in horizontal movement");
        }
        //обратить внимание https://www.udemy.com/unitycourse/learn/lecture/10248916#overview
    }

    #endregion

    #region Public Methods

    public void Die()
    {
        if (IsAlive)
        {
            _rigidbody2D.AddForce(_collisionWithEnemyMotionVector, ForceMode2D.Impulse);
            IsAlive = false;
            _animator.SetTrigger("Die");
            Debug.Log("<color=red><b>The player died</b></color>");
        }
    }

    #endregion

}
