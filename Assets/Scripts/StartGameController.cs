﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartGameController : MonoBehaviour
{
    #region Unoty Editor

    [SerializeField] private Button _startButton;
    [SerializeField] private TMP_Text _recordText;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _startButton.onClick.AddListener(() =>
        {
            SceneLoadingController.LoadNextScene();
        });
    }

    private void Start()
    {
        GetRecord();
    }

    #endregion

    #region Private Methods

    private void GetRecord()
    {
        if (!PlayerPrefs.HasKey("record"))
        {
            PlayerPrefs.SetInt("record", 0);
        }
        _recordText.text = PlayerPrefs.GetInt("record").ToString();
    }

    #endregion
}
