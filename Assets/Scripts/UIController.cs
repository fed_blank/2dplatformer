﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{

    #region Unity Editor 

    [SerializeField] private PageUI _gamePage;
    [SerializeField] private PageUI _lostGamePage;

    #endregion

    #region Public Methods

    public void OpenGamePage()
    {
        _gamePage.Show();
    }

    public void OpenLostGamePage()
    {
        _lostGamePage.Show();
    }

    public PageUI GetGamePage()
    {
        return _gamePage;
    }

    public PageUI GetLostPage()
    {
        return _lostGamePage;
    }
    #endregion

}
